Brot backen ist ein iterativer Prozess: Man bäckt ein Brot nach einem Rezept, 
verändert und verbessert das Rezept, forkt Varianten. Was wäre besser geeignet, 
als git, um die Sourcen und Rezepte zu verwalten?