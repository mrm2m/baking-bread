# Ciabatta

Ciabatta ist nicht ganz leicht. Der Teig muss sehr flüssig sein, damit er die typischen ungleichen und großen Poren bildet. So ist er allerdings nicht ganz einfach [zu verarbeiten](https://www.youtube.com/watch?v=IJ3rwabNSnc). Bei mir ist der Teig sogar noch etwas weicher als im Video. Es könnte helfen, anstelle des Weizenmehl Type 550 italienisches Weizenmehl oder "doppelt griffiges" Weizenmehl zu verwenden. 

## Zutaten

| Menge  | Zutat                         |
| ------:| ----------------------------- |
|   25 g | Roggensauerteig (inaktiv)     |
|  250 g | Weizenmehl Type 550           |
|  180 g | Wasser                        |
| ------ | ----------------------------- |
|  400 g | Weizenmehl Type 550           |
|   70 g | Dinkelvollkornmehl            |
|  300 g | Wasser                        |
| ------ | ----------------------------- |
|   14 g | Salz                          |
|   40 g | Wasser                        |
|   25 g | Olivenöl                      |

Den Weizensauerteig ansetzen und einen halben Tag gehen lassen. Dann den Hauptteig ansetzen und ca. 60 Minuten abgedeckt stehen lassen. Alle Zutaten mischen und den Teig wie im Video zu sehen zu einer homogenen Masse kneten. Teig einen halben Tag in den Kühlschrank stellen. 

Am Backtag den Teig noch einmal falten und auf leicht eingeöhlten Backblechen in Form bringen (ergibt vier Brote). Die Brote 3 Stunden bei Zimmertemperatur aufgehen lassen. Den Backofen mit einer Schale Wasser auf 230°C vorheizen. 25 Minuten Backen, dabei die Temperatur etwas reduzieren.



