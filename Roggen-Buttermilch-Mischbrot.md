# Roggen-Mischbrot mit Buttermilch

Dies ist ein sehr einfaches Rezept, dass eigentlich stets gelingen sollte.

## Zutaten

| Menge  | Zutat                         |
| ------:| ----------------------------- |
|   2 EL | Sauerteig                     |
|  600 g | Roggenmehl Type 1150          |
| 600 ml | Wasser                        |
|  500 g | Roggenmehl Type 815           |
|  500 g | Weizenmehl Type 550           |
|   1 kg | Buttermilch                   |
|   30 g | Salz                          |
|        | Backtrennspray oder Butter    |

Ergibt 3kg Brotteig für Kastenformen.

Zubereitungszeit 3 Tage. Backzeit 50 Minuten bei 180°C Ober- und Unterhitze.

## Zubereitung

Ein Roggenbrot braucht zwei Dinge: Etwas Zeit und einen guten Sauerteig.

### Sauerteig führen

Unter Führen des Sauerteigs versteht man die Zucht und Vermehrung von Milchsäurebakterien und Hefepilzen im 
Sauerteig. Für dieses Brot wird der Sauerteig in mehreren Iterationen  mit Roggenmehl Type 1150 und Wasser 
zu je gleichen Teilen gefüttert. **Achtung: Während des Gährprozesses geht der Teig jeweils auf etwa die 
vierfache Größe auf. Die Schüssel sollte jeweils entsprechend groß dimensioniert sein.**

Am ersten Tag werden zwei Esslöffel Sauerteig mit jeweils 100 Gramm Roggenmehl und 200 ml Wasser gemischt.
Der Sauerteig hat es gerne etwas wärmer und ohne Zugluft. Ich stelle ihn während der Gesamten Zubereitung 
immer wieder in den Ofen: Backofenbeleuchtung einschalten und die Tür mit einem Kochlöffel einen Spalt offen 
halten. Die Lampe sollte ausreichen, um den Ofen auf etwas über Raumtemperatur zu halten.
Über 12 Stunden sollte der Sauerteig auf etwa das vierfache Volumen anwachsen. Einige Stunden später fällt er
wieder etwas in sich zusammen. Je nach temperatur kann das auch schneller gehen. Der Sauerteig sollte sauer 
riechen gerne nach allen möglichen Gewürzen, Blumen oder früchten duften, aber er darf nicht verdorben 
richen. Ist das passiert, dann hat der Sauerteig es leider nicht geschafft, sich gegen Fäulnis-Bakterien 
durchzusetzen. Mir ist das allerdings bisher nicht vorgekommen. 

Nach 12-24 Stunden, wenn er schon eine weile lang nicht mehr wächst, werden erneut 200 Gramm Roggenmehl und 
200 ml Wasser hinzu gegeben. Diesmal wird der Teig etwas schneller gehen. 

Nach ca. 12 Stunden ist der Teig wieder etwas in sich zusammen gefallen und wird jetzt noch einmal mit 300gr
Roggenmehl und 300ml Wasser angesetzt. 

Wenn der Teig erneut gegangen ist haben wir etwas mehr als 1 kg Sauerteig, da ein bisschen Wasser verdunstet 
ist. Wir nehmen etwas vom Teig ab und verwahren es in einem kleinen luftdichten Glas für den nächsten Teig im
Kühlschrank.

## Backtag

Sauerteig, Roggenmehl Type 815 und Weizenmehl, Buttermilch und Salz vermengen. Entweder von Hand oder in 
einer Knetmaschine, dabei immer wieder abwechselnd Mehl und Buttermilch zugeben, sodass sich möglichst keine
klumpen bilden. Der Teig hat zum Schluss eine zähe, fast flüssige konsistenz. Die Backformen entweder sparsam
mit Butter oder mit Backtrennspray einfetten und dann den Teig hinein geben. Er sollte etwa ein drittel der 
Höhe der Form ausfüllen.

Den fertigen Teig in der Form in den Backofen stellen und wie zuvor auch das Licht als Wärmequelle nutzen. 
Der teig geht jetzt etwas scheller, aber nicht so stark auf, sodass er nach etwa 3-5 Stunden knapp unter der 
oberen Kante der Form wieder beginnt etwas einzufallen. Jetzt ist der Zeitpunkt zum Backen gekommen. Tür 
schließen und den Ofen auf 180°C Ober- und Unterhitze einstellen. Alle 20 Minuten nachschauen, ob der Teig zu 
dunkel wird und ggf. etwas herunter stellen und die Formen abdecken. Bei meinem Ofen passt das aber so. Nach 
50 Minuten den Ofen Abstellen.
