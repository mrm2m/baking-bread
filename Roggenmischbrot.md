# Roggenmischbrot

Dies ist ein Roggenmischbrot, das als 1,5kg-Leib gebacken wird. 

## Zutaten

| Menge   | Zutat                                  |
| -------:| -------------------------------------- |
| 1,25 kg | Roggensauerteig über zwei Tage geführt |
| 50 g    | Zucker                                 |
| 50 g    | Salz (knapp)                           |
| 500 g   | Weizenmehl (Type 550)                  |
| 750 g   | Roggenmehl (Type 970)                  |
| 500 ml  | Wasser                                 |

## Zubereitung

Zutaten zu einem geschmeidigen Teig verkneten und in bemehlten Gehrkörben in zwei Leib aufgehen lassen. Dann auf einem gefetteten oder mit Backpapier ausgelegten Blech ca. 60 Minuten bei 200°C backen. Das Brot ist fertig gebacken, wenn es beim darauf klopfen hohl klingt und an einem Zahnstocher kein Teig mehr kleben bleibt.
